//
//  ViewController.swift
//  APITester
//
//  Created by Alexander Khodko on 10/09/2017.
//  Copyright © 2017 Alexander Khodko. All rights reserved.
//

import UIKit
import Alamofire

class ViewController: UIViewController {

    @IBOutlet var resultLable: UILabel!
    @IBOutlet var addressTextField: UITextField!
    @IBOutlet var methodTextField: UITextField!
    @IBOutlet var resultTextField: UITextView!
    
    @IBAction func testMe(_ sender: UIButton) {
        var requestMethod: HTTPMethod!
        if let method = methodTextField.text {
            switch method {
                case "get": requestMethod = .get
                case "post": requestMethod = .post
                case "put": requestMethod = .put
                case "delete": requestMethod = .delete
            default:
                let alert = UIAlertController(title: "Ты как метод ввел", message: "????", preferredStyle: .actionSheet)
                let action = UIAlertAction(title: "Да, да, понял", style: .default, handler: nil)
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
                return
            }
        } else {
            let alert = UIAlertController(title: "Введи метод", message: "????", preferredStyle: .actionSheet)
            let action = UIAlertAction(title: "Да, да, понял", style: .default, handler: nil)
            alert.addAction(action)
            self.present(alert, animated: true, completion: nil)
            return
        }
        var requestURL: String!
        if let url = self.addressTextField.text {
            requestURL = url
        } else {
            
            
            
            let alert = UIAlertController(title: "А URL где?", message: "????", preferredStyle: .actionSheet)
            let action = UIAlertAction(title: "Да, да, понял", style: .default, handler: nil)
            alert.addAction(action)
            self.present(alert, animated: true, completion: nil)
            return
        }
        
        Alamofire.request(requestURL, method: requestMethod, parameters: nil, encoding: JSONEncoding.default, headers: nil).validate().responseJSON(completionHandler: { response in
            if response.error == nil {
                self.resultTextField.text = "Status code : \(response.response!.statusCode)."
                self.resultTextField.text = self.resultLable.text! + "\nJSON ответ: " + "\(response.result.value ?? "А JSON'а то нет!??!?")"
            } else {
                self.resultTextField.text = response.error!.localizedDescription
            }
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        self.view.addGestureRecognizer(tapGesture)
    }
    
    @objc private func hideKeyboard() {
        self.view.endEditing(true)
    }
}

